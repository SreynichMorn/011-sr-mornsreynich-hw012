import React from 'react'
import {Link} from 'react-router-dom';
import queryString from 'query-string';
function Account(props) {

    let pv=queryString.parse(props.location.search)
    return (
        <div className="container">
            <h1 style={{textAlign:'left',marginLeft:130}}>Account</h1>
            <p style={{textAlign:'left',marginLeft:160}}>
                <ul>
                    <li><Link to={`/Account?name=netfliz`}>Netflix</Link></li>
                    <li><Link to={`/Account?name=zillow-group`}>Zillow Group</Link></li>
                    <li><Link to={`/Account?name=yahoo`}>Yahoo</Link></li>
                    <li><Link to={`/Account?name=modus-create`}>Modus Create</Link></li>    
                </ul>
            </p>  
            {pv.name == undefined ? (<h2 style={{textAlign:'left',marginLeft:70}}>There is no name in the query String</h2>):(<h2 style={{textAlign:'left',marginLeft:70}}>The name in the query string is "{pv.name}"</h2>)}
        </div>

     )
    
}

export default Account
