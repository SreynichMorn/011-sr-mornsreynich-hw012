import React from 'react'
import {Link, Route,Switch} from 'react-router-dom';
import Action from './Action';
import Romance from './Romance';
import Comedy from './Comedy';
function Animation() {
    return (
            <div className="container">
                <h1>Animation Category</h1>
                <ul>
                    <li><Link to={`/Video/Animation/Action`}>Action</Link></li>
                    <li> <Link to={`/Video/Animation/Romance`}>Romance</Link></li>
                    <li><Link to={`/Video/Animation/Comedy`}>Comedy</Link></li>
                </ul>
                
                <Switch>
                    <Route path="/Video/Animation/Action" exact component={Action}></Route>
                    <Route path="/Video/Animation/Romance"  component={Romance}></Route>
                    <Route path="/Video/Animation/Comedy"  component={Comedy}></Route>
                </Switch>
            </div>
    )
}

export default Animation
