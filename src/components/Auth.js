import React from 'react';
import {Form,Button,Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
function Auth(props) {
    return (
        <div>
            <br />
            < Form onSubmit={props.handleSubmit}>
                <div className="container">
                    <Form.Row>
                        <Col>
                        <Form.Control placeholder="Input User" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="Input Password" />
                        </Col>
                        <Link as={Link} to="/welcome" >
                            <Button variant="primary" type="submit" >
                                Submit
                            </Button>
                        </Link>
                    </Form.Row>
                </div>
            </Form>
        </div>
    )
}

export default Auth
