import React from 'react'
import { Route ,Switch} from 'react-router-dom'
import Movie from './Movie';
import Animation from './Animation';
import {Link} from 'react-router-dom';

function Video() {
  return (
    
    <div className="container">
        <ul>
          <li><a><Link to={`/Video/Animation`}>Animation</Link></a></li>
          <li><Link to={`/Video/Movie`}>Movie</Link></li>
        </ul>

        <Switch>
            <Route exact path="/Video"><h2>Please select a topic</h2></Route>
            <Route path="/video/Animation" component={Animation}></Route>
            <Route path="/video/Movie" component={Movie}></Route>
        </Switch>
       
    </div>
  )
}

export default Video
