import React from 'react'
import {Navbar,Nav,Form,FormControl,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Menu() {
    return (
        <div>

            <Navbar bg="light" expand="lg">
                <div className="container">
                    <Navbar.Brand as= {Link} to="/">React Router</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        
                            <Nav.Link as={Link} to="/Home">Home</Nav.Link>
                            <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                            <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                            <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
                        </Nav>
                        <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-success">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </div>
            </Navbar>
  
        </div>
    )
}
