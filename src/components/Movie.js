import React from 'react'
import {Link} from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import Adventure from './Adventure';
import Crime from './Crime';
import Documentary from './Documentary';
import Comedy from './Comedy';
function Movie() {
    return (
       
           <div className="container"> 
                <h1>Movie Category</h1>
                <ul>
                    <li><Link to={`/Video/Movie/Adventure`}>Adventure</Link></li>
                    <li><Link to={`/Video/Movie/Crime`}>Crime</Link></li>
                    <li><Link to={`/Video/Movie/Documentary`}>Documentary</Link></li>
                    <li><Link to={`/Video/Movie/Comedy`}>Comedy</Link></li>
                </ul>
            
                <Switch>
                    <Route path="/Video/Movie/Adventure" exact component={Adventure}></Route>
                    <Route path="/Video/Movie/Crime" component={Crime}></Route>
                    <Route path="/Video/Movie/Documentary" component={Documentary}></Route>
                    <Route path="/Video/Movie/Comedy"  component={Comedy}></Route>
                </Switch>
            </div>
        
    )
}

export default Movie
