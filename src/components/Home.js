import React from 'react'
import {Card,Button,CardDeck} from 'react-bootstrap';
import {Link} from 'react-router-dom';
function Home() {
    return (
        <div className="container">
            <div className="row">
               
                <div className="col-md-12">
                    <CardDeck>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic1.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/1`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic2.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This card has supporting text below as a natural lead-in to additional
                                content.{' '}
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/2`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic3.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action.
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/3`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        
                    </CardDeck>
                </div>

                <div className="col-md-12">
                    <CardDeck>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic1.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/4`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic2.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This card has supporting text below as a natural lead-in to additional
                                content.{' '}
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/5`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic3.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action.
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/6`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        
                    </CardDeck>
                </div>

                <div className="col-md-12">
                    <CardDeck>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic1.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/7`} >See More</Link></p>
                            </Card.Body>
                            
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic2.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This card has supporting text below as a natural lead-in to additional
                                content.{' '}
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/8`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={require('../images/pic3.jpg')} />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action.
                            </Card.Text>
                            <p style={{textAlign:'left',marginLeft:10}}><Link  to={`/post/9`} >See More</Link></p>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        
                    </CardDeck>
                </div>
            </div> 
        </div>
    )
}

export default Home

