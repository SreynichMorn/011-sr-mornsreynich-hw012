import React from 'react';
import  { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Menu from './components/Menu';
import home from './components/Home';
import Video from './components/Video';
import account from './components/Account';
import auth from './components/Auth';
import main from './components/Main';
import welcome from './components/Welcome';
import post from './components/Post';

function App() {
  const [setUser] = useState(false)

  const  handleSubmit= e => {
    e.preventDefault();
    setUser(true);
  }
  return (
    <div>
      <Router>
         <Menu />
         <Switch>

          <Route path='/' exact component={main}></Route>
          <Route path='/home'  component={home}></Route>
          <Route path='/Video'   component={Video}></Route>
          <Route path='/account' component={account}></Route>
          <Route path='/auth' component={auth}></Route>
          <Route path='/welcome' render  component={welcome}></Route>
          <Route path='/post/:id' component={post}></Route>   
         </Switch>
         
      </Router>
    </div>
  );
}

export default App

